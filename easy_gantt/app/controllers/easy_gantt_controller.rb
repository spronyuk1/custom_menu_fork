class EasyGanttController < ApplicationController
  accept_api_auth :index, :issues, :projects, :change_issue_relation_delay

  before_filter :find_optional_project
  before_filter :find_query, :only => [:issues, :projects]
  before_filter :authorize, :if => Proc.new{ @project.present? }
  before_filter :authorize_global, :if => Proc.new{ @project.nil? }

  menu_item :easy_gantt

  helper :queries
  helper :easy_gantts
  helper :custom_fields
  if defined?(EasyExtensions)
    helper :easy_query
    include EasyQueryHelper
  end
  include SortHelper

  def index
    redirect_to :issues
  end

  def issues
    if Setting.rest_api_enabled != "1"
      render_error :message => l(:no_rest_api, :scope => [:easy_gantt, :errors])
    else
      find_start_date
      find_end_date

      respond_to do |format|
        format.html { render(:layout => !request.xhr?) }
        format.api do
          find_fixed_versions
          find_projects
          find_relations
          find_entities
        end
      end
    end
  end

  def projects
    project_scope = Project.column_names.include?('easy_is_easy_template') ? Project.where(:easy_is_easy_template => false) : Project
    project_ids = @query.create_entity_scope.distinct.pluck(:project_id)

    @projects = project_scope.visible.preload(:enabled_modules).has_module(:issue_tracking).where(Project.table_name => { :id => project_ids })

    respond_to do |format|
      format.api
    end
  end

  def change_issue_relation_delay
    return render_403 unless User.current.allowed_to?(:manage_issue_relations, @project, :global => true)
    find_relation
    @relation.update_attributes(:delay => params[:delay])
    respond_to do |format|
      format.api { @relation.valid? ? render_api_ok : render_api_errors(@relation.errors.full_messages) }
    end
  end

  private

  def find_relation
    @relation = IssueRelation.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render_404
  end

  def query_class
    EasyGantt::EasyGanttIssueQuery
  end

  def find_query
    if defined?(EasyIssueGanttQuery)
      params[:force_use_from_session] = true
      retrieve_query(EasyIssueGanttQuery, :use_session_store => true)
      @query.export_formats = {}
      @query.display_filter_group_by_on_index = false
      @query.display_filter_settings_on_index = false
      @query.group_by = nil
    else
      find_query_from_params_or_session
    end
  end

  def find_query_from_params_or_session
    if !params[:query_id].blank?
      cond = 'project_id IS NULL'
      cond << " OR project_id = #{@project.id}" if @project
      @query = query_class.where(cond).find(params[:query_id])
      raise ::Unauthorized unless @query.visible?
      @query.project = @project
      session[:query] = {:id => @query.id, :project_id => @query.project_id}
      sort_clear
    elsif params[:set_filter] || session[:query].nil? || session[:query][:project_id] != (@project ? @project.id : nil)
      # Give it a name, required to be valid
      @query = query_class.new(:name => '_')
      @query.project = @project
      @query.build_from_params(params)
      session[:query] = {:project_id => @query.project_id, :filters => @query.filters, :group_by => @query.group_by, :column_names => @query.column_names}
    else
      # retrieve from session
      @query = nil
      @query = query_class.find_by_id(session[:query][:id]) if session[:query][:id]
      @query ||= query_class.new(:name => '_', :filters => session[:query][:filters], :group_by => session[:query][:group_by], :column_names => session[:query][:column_names])
      @query.project = @project
    end
    @query.group_by = nil
  end

  def find_fixed_versions
    if @project
      scope = if Setting.display_subprojects_issues?
        Version.where("id IN (#{@project.shared_versions.select(:id).to_sql}) OR id IN (#{@project.rolled_up_versions.visible.select(:id).to_sql})").uniq
      else
        @project.shared_versions
      end
      @fixed_versions = scope.reorder(:effective_date)
    else
      fixed_version_scope = Version.visible.open
    end

    if Project.column_names.include?('easy_is_easy_template')
      @fixed_versions = fixed_version_scope.where(Project.table_name => {:easy_is_easy_template => false}).reorder(:effective_date) if fixed_version_scope
    else
      @fixed_versions = fixed_version_scope.reorder(:effective_date) if fixed_version_scope
    end

    if !@project && Project.column_names.include?('easy_baseline_for_id')
      @fixed_versions = @fixed_versions.where(Project.table_name => {easy_baseline_for_id: nil})
    end
  end

  def find_projects
    project_scope = Project
    if Project.column_names.include?('easy_is_easy_template')
      project_scope = project_scope.where(:easy_is_easy_template => false)
    end
    @projects = project_scope.visible.has_module(:issue_tracking).where(:id => @query.create_entity_scope(:includes => [:project, :status, :assigned_to, :fixed_version, :tracker, :priority, :custom_values]).pluck(:project_id))
  end

  def find_relations
    @relations = IssueRelation.joins(:issue_from => [:project, :status]).where(@query.statement)

    if !@project && Project.column_names.include?('easy_baseline_for_id')
      @relations = @relations.where(Project.table_name => {easy_baseline_for_id: nil})
    end
  end

  def find_entities
    @entities = @query.entities(:includes => [:project, :status, :assigned_to, :fixed_version, :tracker, :priority, :custom_values], :order => "#{Issue.table_name}.start_date", :preload => [:project, :author, :assigned_to, :relations_from, :relations_to])
  end

  def find_start_date
    @start_date = @query.entity.minimum(:start_date)
    if @start_date.nil? && (x = @query.entity.minimum(:due_date))
      @start_date = x - 1.day
    end
    @start_date ||= Date.today
  end

  def find_end_date
    @end_date = @query.entity.maximum(:start_date)
  end

  def authorize(ctrl = params[:controller], action = params[:action], global = false)
    if @project.nil? || @project.module_enabled?(:easy_gantt)
      super
    else
      non_gantt_project = @project
      @project = nil
      if super(ctrl, action, true)
        @project = non_gantt_project
        true
      else
        false
      end
    end
  end

  def find_optional_project
    #easy query workaround
    return if params[:set_filter] == '1' && params[:project_id] && params[:project_id].match(/\A(=|\!|\!\*|\*)\S*/)

    if params[:no_turn_on_check] == '1'
      @project = Project.visible.find(params[:project_id]) unless params[:project_id].blank?
    else
      super
    end
  rescue ActiveRecord::RecordNotFound
    render_404
  end

end
