##### CustomMenu ttgrtr

#### Plugin for Redmine

Plugin that greatly improves usablity of Redmine.
It implements a lof of things that made Redmine interface user-friendly.


#### Copyright
Copyright (c) 2011-2013 Vladimir Pitin, Danil Kukhlevskiy.

Another plugins of our team you can see on site http://rmplus.pro

Changelog:
  1.5.1:
    * support relative urls
  1.5.0:
    * support redmine 3.0