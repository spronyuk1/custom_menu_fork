require 'custom_menu/awesome_nested_set_patch'

Redmine::Plugin.register :custom_menu do
  name 'Custom Menu Redmine'
  author 'Kovalevskiy Vasil, Danil Kukhlevskiy'
  description 'This is a plugin for Redmine changable top menu'
  version '1.5.1'
  url 'http://rmplus.pro/'
  author_url 'http://rmplus.pro/'

  settings partial: 'settings/custom_menu',
           default: {}

  menu :custom_menu, :cm_all_projects, {controller: 'projects', action: 'index'}, caption: :cm_all_projects, if: Proc.new { User.current.logged? }, html: {class: 'no_line'}
  menu :custom_menu, :my_name, '', caption: Proc.new { ('<span>' + User.current.name + '</span>').html_safe }, if: Proc.new { User.current.logged? }, html: {class: 'in_link'}
  menu :custom_menu, :cm_search, nil, caption: Proc.new { CmItem.cm_search }, if: Proc.new { true }
  menu :custom_menu, :cm_project_tree, nil, caption: Proc.new { CmItem.redner_menu_projects_tree }, if: Proc.new { User.current.logged? }
  menu :custom_menu, :cm_my_activity, nil, caption: Proc.new { CmItem.cm_my_activity }, if: Proc.new { User.current.logged? }


  menu :admin_menu, :cm_menu, { controller: 'cm_menu', action: 'index' }, caption: :label_cm_admin_menu_item
end

Redmine::MenuManager::MenuItem.send(:include, CustomMenu::MenuItemPatch)
Redmine::MenuManager::MenuHelper.send(:include, CustomMenu::MenuManagerPatch)

Rails.application.config.after_initialize do
  plugins = { a_common_libs: '1.1.3' }
  plugin = Redmine::Plugin.find(:custom_menu)
  plugins.each do |k,v|
    begin
      plugin.requires_redmine_plugin(k, v)
    rescue Redmine::PluginNotFound => ex
      raise(Redmine::PluginNotFound, "Plugin requires #{k} not found")
    end
  end
end

require 'custom_menu/view_hooks'